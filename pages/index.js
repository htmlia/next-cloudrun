function Home() {
  return (
    <div className="w-full min-h-screen flex flex-col">
      <header>Home</header>
      <main className="flex-1 bg-red-50">Main</main>
      <footer>Footer</footer>
    </div>
  );
}

export default Home;
